package com.ling.test.db;

import com.ling.test.structure.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class TODOServiceSQLite implements TODOService {

    @Autowired
    SQLiteConnector connector;

    @Override
    public TODO AddTODO(TODO todo) {
        try {

            if(connector.getTodoDao().create(todo) > 0){
                return todo;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException("TODOServiceSQLite.AddTODO - Error creating TODO : " + e.getMessage());
        }
    }

    @Override
    public TODO getTODOById(int id) {
        try {
            return connector.getTodoDao().queryForId(id);
        } catch (SQLException e) {
            throw new RuntimeException("TODOServiceSQLite.getTODOById - Error getting TODO : " + e.getMessage());
        }
    }

    @Override
    public List<TODO> getTODOByWho(String who) {
        try {
            return connector.getTodoDao().queryForEq("who", who);
        } catch (SQLException e) {
            throw new RuntimeException("TODOServiceSQLite.getTODOByWho - Error getting TODO : " + e.getMessage());
        }

    }

    @Override
    public TODO updateTODO(TODO todo) {
        try {

            if(connector.getTodoDao().update(todo) > 0){
                return todo;
            }

            return null;
        } catch (SQLException e) {
            throw new RuntimeException("TODOServiceSQLite.updateTODO - Error update TODO : " + e.getMessage());
        }

    }

    @Override
    public boolean deleteTODOById(int id) {
        try {
            return connector.getTodoDao().deleteById(id) > 0;
        } catch (SQLException e) {
            throw new RuntimeException("TODOServiceSQLite.deleteTODOById - Error delete TODO : " + e.getMessage());
        }
    }
}
