package com.ling.test.rest;


import com.ling.test.db.TODOService;
import com.ling.test.structure.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@RequestMapping("/v1")
@RestController
public class TODOAccessPoint {


    @Autowired
    TODOService service;

    @PostMapping(value = "/todo")
    public @ResponseBody ResponseEntity<TODO> createNewTodo(@RequestBody @Valid TODO todo){
        todo = service.AddTODO(todo);
        if(todo == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(todo,HttpStatus.CREATED);
    }


    @PutMapping(value = "/todo/{id}")
    public @ResponseBody ResponseEntity<TODO> updateTodo(@PathVariable(value="id") int id, @RequestBody @Valid TODO todo){
        todo.setId(id);
        todo = service.updateTODO(todo);
        if(todo == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(todo,HttpStatus.OK);
    }

    @GetMapping(value = "/todo/{id}")
    public @ResponseBody ResponseEntity<TODO> getTodoByID(@PathVariable(value="id") int id){
        TODO todo = service.getTODOById(id);
        if(todo == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(todo,HttpStatus.OK);
    }

    @GetMapping(value = "/todos/{who}")
    public @ResponseBody ResponseEntity<List<TODO>> getTodoByWho(@PathVariable(value="who") String who){
        List<TODO> todos = service.getTODOByWho(who);
        if(todos == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(todos,HttpStatus.OK);
    }

    @DeleteMapping(value = "/todo/{id}")
    public ResponseEntity deleteTODO(@PathVariable(value="id") int id){
        if(!service.deleteTODOById(id)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }





    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleNotEmptyException(MethodArgumentNotValidException exception) {

        StringBuilder errors =  new StringBuilder();

        exception.getBindingResult().getAllErrors().forEach(error -> errors.append(error.getDefaultMessage() + ", " ));

        //Remove last ", " for more beautiful response
        errors.delete(errors.length() - 2,errors.length());

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errors.toString());
    }


}
