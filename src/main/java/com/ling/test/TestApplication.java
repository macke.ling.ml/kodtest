package com.ling.test;

import com.ling.test.db.SQLiteConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import javax.annotation.PreDestroy;

@SpringBootApplication
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}

	@Autowired
	SQLiteConnector connector;

	@PreDestroy
	public void onDestroy() throws Exception {
		connector.closeConnection();
		System.out.println("Closed SQLite connection source");
	}
}
