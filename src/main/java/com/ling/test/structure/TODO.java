package com.ling.test.structure;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import javax.validation.constraints.NotEmpty;


@DatabaseTable(tableName = "TODO")
public class TODO {

    @DatabaseField(generatedId = true, unique = true)
    private int id;

    @DatabaseField(canBeNull = false)
    @NotEmpty(message = "missing required data 'who'")
    private String who;

    @DatabaseField(canBeNull = false)
    @NotEmpty(message = "missing required data 'description'")
    private String description;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getWho() {
        return who;
    }
}
