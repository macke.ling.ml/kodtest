package com.ling.test.db;

import com.ling.test.structure.TODO;

import java.util.List;

public interface TODOService {

    TODO AddTODO(TODO todo);

    TODO getTODOById(int id);

    List<TODO> getTODOByWho(String who);

    TODO updateTODO(TODO todo);

    boolean deleteTODOById(int id);

}
