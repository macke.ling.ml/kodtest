package com.ling.test.db;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.ling.test.structure.TODO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import java.sql.SQLException;

@Component
public class SQLiteConnector {

    private static JdbcPooledConnectionSource connectionSource =  null;

    private static Dao<TODO, Integer> todoDao;

    public SQLiteConnector(@Value("${SQLite.path}") String path){

        if(connectionSource == null){
            try {
                connectionSource = new JdbcPooledConnectionSource("jdbc:sqlite:" +path );

                TableUtils.createTableIfNotExists(connectionSource, TODO.class);

                todoDao = DaoManager.createDao(connectionSource, TODO.class);

            }catch (SQLException e){
                throw new RuntimeException("SQLiteConnector - Error setup DB : " + e.getMessage());
            }
        }
    }

    public Dao<TODO, Integer> getTodoDao(){
        return todoDao;
    }


    public void closeConnection(){
        if(connectionSource != null){
            connectionSource.closeQuietly();
        }
    }
}
