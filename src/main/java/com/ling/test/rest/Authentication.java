package com.ling.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;


@Configuration
@EnableWebSecurity
public class Authentication extends WebSecurityConfigurerAdapter {

    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.csrf().disable();

        http.authorizeRequests()
                .anyRequest().authenticated()
                .and().httpBasic();
    }


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, @Value( "${auth.username}") String username, @Value( "${auth.password}")String password) throws Exception {

        auth.inMemoryAuthentication()
                .withUser(username).password("{noop}"+ password).roles("USER");
    }

}

